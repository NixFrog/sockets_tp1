#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"

#define BUFFER_SIZE 256

int main(int argc, const char* argv[]) {
	if(argc != 2){
		perror("Server: Incorrect number of arguments\nUsage: port\n");
		exit(EXIT_FAILURE);
	}

	int sockfd;
	struct sockaddr_in serverAddress;
	char msgBuffer[BUFFER_SIZE];

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd == -1){
		perror("Server error: socket(): failed to create socket");
		return EXIT_FAILURE;
	}

	// Prepare server address
	bzero((char *) &serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(atoi(argv[1]));
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	memset(serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));

	if(bind(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1){
		perror("Server error: bind(): failed to bind socket");
		return(EXIT_FAILURE);
	}

	// Listen for all incoming messages as long as it doesn't fail
	while(1){
		struct sockaddr_in clientAddress;
		char *extractedMessage, *response;
		int receivedMsgSize;
		socklen_t clientAddressSize;

		clientAddressSize = sizeof(clientAddress);

		if(receiveMessage(sockfd, msgBuffer, BUFFER_SIZE, &receivedMsgSize, (struct sockaddr *) &clientAddress, &clientAddressSize) == -1){
			continue;
		}

		printf("Server: %d bytes received:\t \"%s\"\n", receivedMsgSize, msgBuffer);

		extractedMessage = extractMsg(msgBuffer, receivedMsgSize);
		response = prepareMsg(extractedMessage);

		if(sendCompleteMessage(sockfd, response, strlen(response)+1, (struct sockaddr *)&clientAddress) == -1){
			printf("Server error: could not send the message to %s\n", inet_ntoa(clientAddress.sin_addr));
			return(EXIT_FAILURE);
		}

	}

	close(sockfd);

	return 0;
}