#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "tools.h"

#define BUFFER_SIZE 256

int main(int argc, const char* argv[]) {
	if (argc < 4) {
		perror("Incorrect number of arguments\nUsage: hostname port message\n");
		return EXIT_FAILURE;
	}

	int sockfd, msgSize, receivedMsgSize;
	char* msg, *receptionBuffer;
	struct sockaddr_in serverAddress;
	struct hostent *server;

	// Get server info
	server = gethostbyname(argv[1]);
	if(server == NULL){
		perror("Client error: gethostbyname(): incorrect host name");
		return EXIT_FAILURE;
	}
	
	// Create socket
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd == -1){
		perror("Client error: socket(): failed to create socket");
		return EXIT_FAILURE;
	}

	// Prepare destination adress
	bzero((char *) &serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(atoi(argv[2]));
	serverAddress.sin_addr = *((struct in_addr*) server->h_addr_list[0]);
	memset(serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));

	// Send message
	msg = prepareMsg(argv[3]);
	msgSize = strlen(msg) + 1;
	if (sendCompleteMessage(sockfd, msg, msgSize, (struct sockaddr*) &serverAddress) == -1) {
		printf("Client error: sendto(): could not send the whole message");
	}

	// Receive message
	receptionBuffer = malloc(BUFFER_SIZE);
	receiveMessage(sockfd, receptionBuffer, BUFFER_SIZE, &receivedMsgSize , NULL, NULL);
	printf("Client: %d bytes received:\t %s\n", receivedMsgSize, receptionBuffer);
	
	free(msg);
	free(receptionBuffer);
	close(sockfd);

	return 0;
}