#ifndef __TOOLS_H__
#define __TOOLS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

char *prepareMsg(const char *msg);
char *extractMsg(char *msg, size_t messageSize);
int sendCompleteMessage(int sockfd, char* msg, int msgSize, const struct sockaddr *serverAddress);
int receiveMessage(int sockfd, char *receptionBuffer, int bufferSize, int *receivedMsgSize, struct sockaddr *incomingAddress, socklen_t *addressSize);

#endif // __TOOLS_H__