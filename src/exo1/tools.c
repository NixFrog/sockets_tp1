#include "tools.h"

char* prepareMsg(const char* msg) {
	char* preparedMsg;

	int pidSize = snprintf(NULL, 0, "%d", getpid());
	int msgSize = sizeof(char) * (pidSize + strlen(msg) + 3);
	preparedMsg = malloc(msgSize);

	snprintf(preparedMsg, msgSize, "%d: %s", getpid(), msg);

	return preparedMsg;
}

char *extractMsg(char *msg, size_t messageSize){
	int pid;
	char *extractedMessage = malloc(sizeof(char) * messageSize);
	sscanf(msg, "%d: %s", &pid,  extractedMessage);

	return extractedMessage;
}

int sendCompleteMessage(int sockfd, char* msg, int msgSize, const struct sockaddr *serverAddress){
	int sentMsgSize = 0;

	while (sentMsgSize < msgSize) {
		int remainingSize = msgSize - sentMsgSize;
		int tmpSize = sendto(sockfd, msg + sentMsgSize, remainingSize, 0, serverAddress, sizeof(*serverAddress));
		if (tmpSize == -1) {
			perror("Client error: sendto(): unable to send the message");
			break;
		}

		sentMsgSize += tmpSize;
	}

	return (sentMsgSize != msgSize) ? -1 : 0;
}

int receiveMessage(int sockfd, char *receptionBuffer, int bufferSize, int *receivedMsgSize, struct sockaddr *incomingAddress, socklen_t *addressSize){
	*receivedMsgSize = recvfrom(sockfd, receptionBuffer, bufferSize, 0, incomingAddress, addressSize);
	
	if (*receivedMsgSize == -1) {
		perror("Client error: receiveMessage");
		return -1;
	}

	if (receptionBuffer[*receivedMsgSize - 1] != '\0') {
		if (*receivedMsgSize == bufferSize) {
			receptionBuffer[*receivedMsgSize - 1] = '\0';
			}
		else {
			receptionBuffer[*receivedMsgSize] = '\0';
		}
	}

	return 0;
}