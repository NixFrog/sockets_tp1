#include <sys/sendfile.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>

#define BUFFER_LEN 1024
#define DEFAULT_PAGE "index.html"

#define HTTP_404_ERROR "HTTP/1.1 404 Not Found\r\n"\
						"Content-Type: text/html\r\n\r\n"\
						"<!DOCTYPE html>\r\n"\
						"<html><body>\r\n"\
						"<h1>Not Found</h1>\r\n"\
						"</body></html>"\

#define TYPE_HTML "text/html"
#define TYPE_PLAIN_TEXT "text/plain"
#define TYPE_OCTET_STREAM "application/octet-stream"

int initServerStreamSocket(int port){
	int sockfd;
	struct sockaddr_in serverAddress;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1){
		perror("Server error: socket initialization");
		exit(EXIT_FAILURE);
	}

	bzero((char *) &serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(port);
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	memset(serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));

	if(bind(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1){
		perror("Server error: socket binding");
		exit(EXIT_FAILURE);
	}

	if(listen(sockfd, 1) == -1){
		perror("Server error: listenning");
		exit(EXIT_FAILURE);
	}

	return sockfd;
}

int logToFile(struct in_addr clientAddress, char *res){
	int logfd;
	char dateString[200];
	char *clientAddressString = inet_ntoa(clientAddress);

	time_t t = time(NULL);
	struct tm *tmp = localtime(&t);
	if(tmp == NULL){
		perror("Error: could not get the local time");
		return -1;
	}

	if(strftime(dateString, 200, "%T", tmp) == 0){
		puts("Error: strftime: failed to write into dateString");
		return -1;
	}

	int stringSize = strlen(clientAddressString) + strlen(dateString) + strlen(res) + 8;
	char *str = malloc(stringSize);

	snprintf(str, stringSize,
		"%s - %s - %s\n",
		clientAddressString, dateString, res
	);

	logfd = open("log.txt", O_CREAT|O_WRONLY|O_APPEND, 0664);
	if(logfd == -1){
		perror("Error: could not open log file");
		free(str);
		return -1;
	}

	write(logfd, str, strlen(str));

	free(str);
	close(logfd);

	return 0;
}


int sendMessage(int sockfd, char* msg, int messageSize){
	int sentSize = 0;

	while(sentSize < messageSize){
		int remainingSize = messageSize - sentSize;
		int tmpSize = send(sockfd, msg + sentSize, remainingSize, 0);
		if(tmpSize == -1){
			perror("Error: failed to send part of the message");
			break;
		}

	    sentSize += tmpSize;
	}

	return(sentSize != messageSize) ? -1 : 0;
}

int sendFile(int fd_src, int fd_dest){
	struct stat statStruct;
	int status;

	status = fstat(fd_src, &statStruct);
	if(status == -1){
		perror("Error: fstat");
		return -1;
	}

	status = sendfile(fd_dest, fd_src, NULL, statStruct.st_size);
	if(status == -1){
		perror("Error: failed to send file");
	}

	return status;
}

char* genereateHTTP_200Response(char *content_type){
	char *buf = malloc(BUFFER_LEN);

	int status = snprintf(buf, BUFFER_LEN,
						  "HTTP/1.1 200 OK\r\n"
						  "Content-Type: %s\r\n\r\n",
						  content_type
						  );
	puts(buf);

	if(status < 0){
		free(buf);
		return NULL;
	}

	return buf;
}

int sendFileByHTTP(char* filepath, int fd_dest){
	int filefd, status;
	char *contentType, *response;

	if(strstr(filepath, ".html") != NULL){
		contentType = TYPE_HTML;
	}
	else if(strstr(filepath, ".txt") != NULL){
		contentType = TYPE_PLAIN_TEXT;
	}else{
		contentType = TYPE_OCTET_STREAM;
	}

	filefd = open(filepath, O_RDONLY);
	if(filefd == -1){
		perror("Error opening required file");

		if(errno == ENOENT){
			sendMessage(fd_dest, HTTP_404_ERROR, strlen(HTTP_404_ERROR));
		}
		return -1;
	}

	response = genereateHTTP_200Response(contentType);
	if(response == NULL){
		close(filefd);
		return -1;
	}
	
	status = sendMessage(fd_dest, response, strlen(response));
	
	free(response);
	
	if(status == -1){
		close(filefd);
		return -1;
	}

	status = sendFile(filefd, fd_dest);

	close(filefd);

	return status;
}

char* processGETBuffer(char *buf){
	char *res = NULL;
	char endline[] = "\r\n";

	if(strstr(buf, endline) != NULL){
		res = malloc(BUFFER_LEN);

		sscanf(buf, "GET %s", res);
	}

	return res;
}

char* receiveGETRequest(int sockfd){
	char buf[BUFFER_LEN];
	int recv_size = 0;
	size_t buf_offset = 0;
	size_t remaining_buf_size = BUFFER_LEN;
	char *res = NULL;
	
	do{
		recv_size = read(sockfd, buf + buf_offset, remaining_buf_size - 1);
		if(recv_size == -1){
			perror("read");
			break;
		}

		buf[buf_offset + recv_size] = '\0';

		res = processGETBuffer(buf);
		if(res != NULL){
			break;
		}

		buf_offset += recv_size;
		remaining_buf_size -= recv_size;
	}while (recv_size != 0 && remaining_buf_size > 0);

	return res;
}

int handleHTTP_GETRequest(int clientfd, struct in_addr clientAddress){
	int status;
	char *path, *resource;

	resource = receiveGETRequest(clientfd);
	if(resource == NULL){
		perror("HTTP_GET error: no resource");
		return -1;
	}

	logToFile(clientAddress, resource);

	if(strcmp(resource, "/") == 0){
		path = DEFAULT_PAGE;
	}
	else {
		path = resource + 1;
	}

	status = sendFileByHTTP(path, clientfd);

	free(resource);

	return status;
}

int printReceivedRequest(int sockfd){
	int receivedSize;
	char buf[BUFFER_LEN];

	do{
		receivedSize = read(sockfd, buf, BUFFER_LEN - 1);
		if(receivedSize == -1){
			perror("Error reading received request");
			break;
		}

		if(receivedSize != 0){
			buf[receivedSize] = '\0';
			printf("%s", buf);
		}

		if(strstr(buf, "\r\n\r\n") != NULL){
			break;
		}
	}while(receivedSize != 0);

	return receivedSize;
}

int main(int argc, const char* argv[]){
	int sockfdHTTP, sockfdLog, status;
	fd_set master, readfds;

	if(argc != 3){
		printf("Invalid arguments\nUsage: http_port log_port\n");
		return EXIT_FAILURE;
	}

	sockfdHTTP = initServerStreamSocket(atoi(argv[1]));
	sockfdLog = initServerStreamSocket(atoi(argv[2]));

	FD_ZERO(&readfds);
	FD_ZERO(&master);
	FD_SET(sockfdHTTP, &master);
	FD_SET(sockfdLog, &master);

	while(1){
		readfds = master;

		status = select(sockfdLog + 1, &readfds, NULL, NULL, NULL);
		if(status == -1){
			perror("Server error: select");
		}
		else if(status != 0){
			if(FD_ISSET(sockfdHTTP, &readfds)){
				struct sockaddr_in clientAddress;
				socklen_t clientAddressLen = sizeof(clientAddress);

				int clientfd = accept(sockfdHTTP, (struct sockaddr *) &clientAddress,
				&clientAddressLen);
				if(clientfd == -1){
					perror("Server error: failed to accept http request");
				}

				handleHTTP_GETRequest(clientfd, clientAddress.sin_addr);

				close(clientfd);
			}

			if(FD_ISSET(sockfdLog, &readfds)){
				int clientfd = accept(sockfdLog, NULL, NULL);
				if(clientfd == -1){
					perror("Server error: logging: failed to accept request");
				}

				printReceivedRequest(clientfd);

				sendFileByHTTP("log.txt", clientfd);

				close(clientfd);
			}
		}
	}

	close(sockfdHTTP);
	close(sockfdLog);

	return EXIT_SUCCESS;
}
