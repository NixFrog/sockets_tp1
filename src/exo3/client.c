#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUFFER_LEN 1024

int initClientStreamSocket(const char* hostname, int port) {
	int sockfd;
	struct hostent *server;
	struct sockaddr_in destinationAddress;

	server = gethostbyname(hostname);
	if(server == NULL){
		perror("Client error: gethostbyname failed");
		exit(EXIT_FAILURE);
	}

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1){
		perror("Client error: initializing socket");
		exit(EXIT_FAILURE);
	}

	bzero((char *) &destinationAddress, sizeof(destinationAddress));
	destinationAddress.sin_family = AF_INET;
	destinationAddress.sin_port = htons(port);
	destinationAddress.sin_addr = *((struct in_addr*) server->h_addr_list[0]);
	memset(destinationAddress.sin_zero, 0, sizeof(destinationAddress.sin_zero));

	if(connect(sockfd, (struct sockaddr*) &destinationAddress, sizeof(destinationAddress)) == -1){
		perror("Client error: connecting to server");
		exit(EXIT_FAILURE);
	}

	return sockfd;
}

char* createHTTP_GETRequest(const char* host, const char* res, const char* port) {
	char *buff = malloc(BUFFER_LEN);

	int status = snprintf(buff, BUFFER_LEN,
		"GET %s HTTP/1.1\r\n"
		"Host: %s:%s\r\n"
		"Connection: close\r\n"
		"Accept: text/html\r\n\r\n",
		res, host, port
	);

	if(status < 0) {
		free(buff);
		return NULL;
	}

	return buff;
}

int sendMessage(int sockfd, char* msg, int messageSize) {
	int sentSize = 0;

	while(sentSize < messageSize){
		int remainingSize = messageSize - sentSize;
		int tmpSize = send(sockfd, msg + sentSize, remainingSize, 0);
		if(tmpSize == -1) {
			perror("Error: failed to send part of the message");
			break;
		}

	    sentSize += tmpSize;
	}

	return(sentSize != messageSize) ? -1 : 0;
}

int printReceivedRequest(int sockfd) {
	int receivedSize;
	char buf[BUFFER_LEN];

	do{
		receivedSize = read(sockfd, buf, BUFFER_LEN - 1);
		if (receivedSize == -1) {
			perror("Error reading received request");
			break;
		}

		if (receivedSize != 0) {
			buf[receivedSize] = '\0';
			printf("%s", buf);
		}

		if (strstr(buf, "\r\n\r\n") != NULL) {
			break;
		}
	}while(receivedSize != 0);

	return receivedSize;
}

int main(int argc, const char* argv[]) {
	int sockfd, status;
	char *request;

	if(argc != 4){
		printf("Invalid arguments\nUsage: hostname port filename\n");
		return EXIT_FAILURE;
	}

	sockfd = initClientStreamSocket(argv[1], atoi(argv[2]));

	request = createHTTP_GETRequest(argv[1], argv[3], argv[2]);
	if(request == NULL){
		printf("Client error: failed to create a GET request");
		return EXIT_FAILURE;
	}
	puts(request);

	status = sendMessage(sockfd, request, strlen(request));
	free(request);
	if(status == -1){
		printf("Client error: failed to send request\n");
		return EXIT_FAILURE;
	}

	if(printReceivedRequest(sockfd) == -1){
		printf("Client error: failed to receive request\n");
		return EXIT_FAILURE;
	}

	close(sockfd);

	return EXIT_SUCCESS;
}
