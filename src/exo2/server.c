#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUFFER_LEN 1024

int initServerStreamSocket(int port) {
	int sockfd;
	struct sockaddr_in serverAddress;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1) {
		perror("Server error: socket initialization");
		exit(EXIT_FAILURE);
	}

	bzero((char *) &serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(port);
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	memset(serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));

	if(bind(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1) {
		perror("Server error: socket binding");
		exit(EXIT_FAILURE);
	}

	if(listen(sockfd, 1) == -1) {
	    perror("Server error: listenning");
	    exit(EXIT_FAILURE);
	}

	return sockfd;
}

int printReceivedRequest(int sockfd) {
	int receivedSize;
	char buf[BUFFER_LEN];

	do{
		receivedSize = read(sockfd, buf, BUFFER_LEN - 1);
		if (receivedSize == -1) {
			perror("Error reading received request");
			break;
		}

		if (receivedSize != 0) {
			buf[receivedSize] = '\0';
			printf("%s", buf);
		}

		if (strstr(buf, "\r\n\r\n") != NULL) {
			break;
		}
	}while(receivedSize != 0);

	return receivedSize;
}

int main(int argc, const char* argv[]) {
	int sockfd, clientfd;

	if(argc != 2) {
		printf("Invalid arguments\nUsage: port\n");
		return EXIT_FAILURE;
	}

	sockfd = initServerStreamSocket(atoi(argv[1]));

	clientfd = accept(sockfd, NULL, NULL);
	if (clientfd == -1) {
	    perror("Server error: accept connection from client");
	    return EXIT_FAILURE;
	}

	printReceivedRequest(clientfd);

	close(clientfd);
	close(sockfd);

	return EXIT_SUCCESS;
}

	