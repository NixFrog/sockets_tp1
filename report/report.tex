\documentclass[a4paper, frenchb, 11pt]{article}

\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}

\usepackage{multicol}
\setlength{\columnseprule}{1pt} % separation line between columns

\usepackage{hyperref}
\hypersetup{
	colorlinks=true,	% false: boxed links; true: colored links
	linkcolor=black,	% color of internal links
	urlcolor=blue,		% color of external links
	citecolor=blue
}

\usepackage{graphicx}	% import graphics
\usepackage{wrapfig}	% wrap text around figures
\usepackage{subcaption}

\usepackage{dirtree} % file tree
% Colors
\usepackage[usenames,dvipsnames]{xcolor}

% Colored frame
\usepackage{mdframed}
\usepackage{framed}
\definecolor{shadecolor}{rgb}{0.96,0.96,0.96}
\definecolor{TFFrameColor}{rgb}{0.96,0.96,0.96}
\definecolor{TFTitleColor}{rgb}{0.00,0.00,0.00}
\definecolor{lightred}{rgb}{1,0.96,0.96}
\definecolor{darkred}{rgb}{0.85,0.33,0.31}
\definecolor{lightblue}{HTML}{EBF5FA}
\definecolor{lightblue2}{HTML}{E3F2FA}
\definecolor{darkblue}{HTML}{D2DCE1}
\definecolor{hintbg}{HTML}{FFFAE6}
\definecolor{hintborder}{HTML}{FAE6BE}

% Redefine leftbar envvironment
\newlength{\leftbarwidth}
\setlength{\leftbarwidth}{1pt}
\newlength{\leftbarsep}
\setlength{\leftbarsep}{10pt}

\newcommand*{\leftbarcolorcmd}{\color{leftbarcolor}} % as a command to be more flexible
\colorlet{leftbarcolor}{gray}

\renewenvironment{leftbar}{%
    \def\FrameCommand{{\leftbarcolorcmd{\vrule width \leftbarwidth\relax\hspace {\leftbarsep}}}}%
    \MakeFramed {\advance \hsize -\width \FrameRestore }%
}{%
    \endMakeFramed
}

% Code listings
\usepackage{listings}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{steelblue}{rgb}{0.16,0.37,0.58}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\definecolor{blue}{rgb}{0,0,0.7}
\lstset{
	language=C,
	basicstyle=\scriptsize,
	numbers=left,                   % where to put the line-numbers
  	numberstyle=\tiny\color{gray},
	commentstyle=\color{steelblue},
	stringstyle=\color{BrickRed},
	backgroundcolor=\color{shadecolor},
    keywordstyle=\color{OliveGreen},
	frame=single,                   % adds a frame around the code
 	rulecolor=\color{black},
	emph={},
	emphstyle=\color{mauve},
	morekeywords=[2]{obside@obsideb},
	keywordstyle={\color{black}},
	keywordstyle=[2]{\color{dkgreen}},
	showstringspaces=false,
  	tabsize=4,
	moredelim=[is][\small\ttfamily]{/!}{!/},
	breaklines=true
}

% Title page
\title{
	\textbf{INF-4201B - TP Sockets}\\
}
\author{
	Bertrand Le Mée\\
	\href{bertrand.lemee@edu.esiee.fr}{\small{bertrand.lemee@edu.esiee.fr}}
}
\date{\today}

\begin{document}
\maketitle
\newpage

\tableofcontents
\newpage


\section{Introduction}
\subsection{Objectifs}
Ce TP a pour but de nous familiariser avec les sockets, dans le but de faire communiquer plusieurs programmes sur le réseau. Nous utilisons deux types de sockets: non connectées (\emph{SOCK\_DGRAM}) et connectées (\emph{SOCK\_STREAM}). Enfin, nous abordons le protocole HTTP en créant un serveur web répondant à des requêtes.

\subsection{Compilation du code}
Les makefiles sont fournis avec le code, mais il peut être nécessaire de les regénérer. Pour cela, il faut utiliser l'utilitaire Premake 4:

\begin{mdframed}[backgroundcolor=shadecolor, linecolor=black]
	premake4 gmake\\
	make
\end{mdframed}

\newpage
\section{Exercice 1}
\subsection{Objectif}
Créer un client et un serveur en mode non connecté avec des \emph{DGRAM\_SOCKETS}. 

Le client se connecte au serveur, et envoie son numéro de PID suivi d'un message passé en argument. Le serveur reçoit le message, l'affiche, puis renvoie au client son propre PID suivi du message reçu, sans le PID du client. Enfin, le client affiche la réponse du serveur.

\subsection{Serveur}
\subsubsection{Création de la socket}
Le serveur crée une socket, puis la lie à toutes les interfaces possibles ainsi qu'au port passé en argument du programme. 
La création d'une socket requiert un appel à \emph{socket()}, la liaison à l'aide de \emph{bind} et de la structure \emph{sockaddr\_in}. Cette structure est spécifique à l'IPv4 et permet d'indiquer les informations suivantes:

\begin{itemize}
\item{sin\_family} La famille d'addresses, ici \emph{AF\_INET} pour l'IPv4
\item{sin\_port} Le port utilisé par la socket, formaté pour le réseau, ici passé en paramètre
\item{sin\_addr} L'addresse IP de l'hôte. Pour le serveur, il faut utiliser \emph{INADDR\_ANY} pour lier la socket à toutes les interfaces locales. \emph{INADDR\_ANY} est représentée par l'addresse \emph{0.0.0.0}. 
\end{itemize}

\begin{lstlisting}
sockfd = socket(AF_INET, SOCK_DGRAM, 0);
if(sockfd == -1){
	perror("Server error: socket(): failed to create socket");
	return EXIT_FAILURE;
}

bzero((char *) &serverAddress, sizeof(serverAddress));
serverAddress.sin_family = AF_INET;
serverAddress.sin_port = htons(atoi(argv[1]));
serverAddress.sin_addr.s_addr = INADDR_ANY;
memset(serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));

if(bind(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1){
	perror("Server error: bind(): failed to bind socket");
	return(EXIT_FAILURE);
}
\end{lstlisting}

\subsubsection{Récupération des données}
Une fois que la socket est initialisée, il est possible de récupérer les données envoyées par le client. C'est le rôle de la fonction \emph{receiveMessage()}. Le message est ensuite affiché, puis formaté pour supprimer le PID du client avec \emph{extractMsg()}, et enfin préparé avec le PID du serveur par la fonction \emph{prepareMsg}. Il ne reste plus qu'à envoyer le message à l'aide de la fonction \emph{sendCompleteMessage()}.


\textbf{Réception d'un message}

\noindent La réception se fait à l'aide de la fonction \emph{receiveMessage()}. Cette fonction utilise \emph{recvfrom()} pour recevoir les données des SOCK\_DGRAM. Si le message ne se termine pas par "\textbackslash 0", alors il est considéré comme étant incomplet.

\begin{lstlisting}
int receiveMessage(int sockfd, char *receptionBuffer, int bufferSize, int *receivedMsgSize, struct sockaddr *incomingAddress, socklen_t *addressSize){
	*receivedMsgSize = recvfrom(sockfd, receptionBuffer, bufferSize, 0, incomingAddress, addressSize);
	
	if (*receivedMsgSize == -1) {
		perror("Client error: receiveMessage");
		return -1;
	}

	if (receptionBuffer[*receivedMsgSize - 1] != '\0') {
		if (*receivedMsgSize == bufferSize) {
			receptionBuffer[*receivedMsgSize - 1] = '\0';
			}
		else {
			receptionBuffer[*receivedMsgSize] = '\0';
		}
	}

	return 0;
}
\end{lstlisting}

\textbf{Formatage d'un message}

\noindent Un message reçu contient le PID de l'émetteur. Ce PID est retiré par la fonction \emph{extractMsg()}. L'envoi d'un message nécessite quant à lui d'ajouter le PID du programme voulant envoyer le message: cela se fait par l'appel à \emph{prepareMsg()}, qui appelle \emph{getpid()} pour obtenir le PID.

\begin{lstlisting}
char* prepareMsg(const char* msg) {
	char* preparedMsg;

	int pidSize = snprintf(NULL, 0, "%d", getpid());
	int msgSize = sizeof(char) * (pidSize + strlen(msg) + 3);
	preparedMsg = malloc(msgSize);

	snprintf(preparedMsg, msgSize, "%d: %s", getpid(), msg);

	return preparedMsg;
}

char *extractMsg(char *msg, size_t messageSize){
	int pid;
	char *extractedMessage = malloc(sizeof(char) * messageSize);
	sscanf(msg, "%d: %s", &pid,  extractedMessage);

	return extractedMessage;
}
\end{lstlisting}

\textbf{Envoi d'un message}

\noindent L'envoi d'un message est fait par la fonction \emph{sendCompleteMessage()}. Cette fonction utilise \emph{sendto()} tant que la taille du message envoyé est différente de la taille du message complet.

\begin{lstlisting}
int sendCompleteMessage(int sockfd, char* msg, int msgSize, const struct sockaddr *serverAddress){
	int sentMsgSize = 0;

	while (sentMsgSize < msgSize) {
		int remainingSize = msgSize - sentMsgSize;
		int tmpSize = sendto(sockfd, msg + sentMsgSize, remainingSize, 0, serverAddress, sizeof(*serverAddress));
		if (tmpSize == -1) {
			perror("Client error: sendto(): unable to send the message");
			break;
		}

		sentMsgSize += tmpSize;
	}

	return (sentMsgSize != msgSize) ? -1 : 0;
}
\end{lstlisting}

\newpage

\subsection{Client}
L'envoi et la réception de message se fait de la même manière pour le client que pour le serveur, j'ai donc créé une fichier tools.h pour regrouper les fonctions nécessaires. Cependant, la configuration du client diffère:

Tout d'abord, le client doit récupérer l'addresse IP du serveur depuis son nom, passé en paramètre. Cela se fait par l'appel à la fonction \emph{gethostbyname()}. 

Une socket est ensuite créée de la même manière que pour le serveur. Il ne reste plus qu'à préparer la structure \emph{sockaddr\_in} contenant l'addresse du serveur. Il faut cette fois utilisé la structure \emph{hostent} renvoyée par \emph{gethostbyname()} pour spécifier l'addresse du serveur.

\begin{lstlisting}
bzero((char *) &serverAddress, sizeof(serverAddress));
serverAddress.sin_family = AF_INET;
serverAddress.sin_port = htons(atoi(argv[2]));
serverAddress.sin_addr = *((struct in_addr*) server->h_addr_list[0]);
memset(serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));
\end{lstlisting}

\subsection{Traces d'exécution}

\begin{figure}[h!]
  \centering
  \includegraphics[width=1\linewidth]{exo1.png}
  \caption{Résultats de la ligne de commande}
\end{figure}


\newpage

\section{Exercice 2}
Il est cette fois demandé de créer un serveur qui peut recevoir une requête HTTP GET envoyée depuis un navigateur web, et qui l'affiche.

\subsection{Serveur}
La socket est cette fois initialisée en tant que SOCK\_STREAM. La structure \emph{sockaddr\_in} est initialisée de la même manière que précédemment. La socket est ensuite marquée comme socket passive, pour écouter les requêtes de connection, avec l'appel à \emph{listen()}.

\begin{lstlisting}
int sockfd;
struct sockaddr_in serverAddress;

sockfd = socket(AF_INET, SOCK_STREAM, 0);
if(sockfd == -1) {
	perror("Server error: socket initialization");
	exit(EXIT_FAILURE);
}

bzero((char *) &serverAddress, sizeof(serverAddress));
serverAddress.sin_family = AF_INET;
serverAddress.sin_port = htons(port);
serverAddress.sin_addr.s_addr = INADDR_ANY;
memset(serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));

if(bind(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1) {
	perror("Server error: socket binding");
	exit(EXIT_FAILURE);
}

if(listen(sockfd, 1) == -1) {
    perror("Server error: listenning");
    exit(EXIT_FAILURE);
}
\end{lstlisting}

Le serveur accepte ensuite une connection avec \emph{accept()}. Une fois une requête reçue, elle est affichée à l'aide de \emph{printReceivedRequest()}. Cette fonction appelle \emph{read()} jusqu'à ce que le message soit entièrement affiché, ou jusqu'à rencontrer \textbackslash r \textbackslash n \textbackslash r \textbackslash n. Cette chaîne de caractères spécifie la fin d'une reqûete HTTP.

\subsection{Requête HTTP}

\begin{figure}[h!]
  \centering
  \includegraphics[width=1\linewidth]{exo2.png}
  \caption{Résultat d'une requête HTTP GET depuis chromium}
\end{figure}

Un navigateur web se connecte au serveur à l'addresse localhost:<port du serveur>. 

La première ligne de la requête indique la ressource à obtenir et le protocole utilisé, ici "/" et HTTP/1.1. La seconde ligne indique l'hôte, localhost, et le port. Les autres lignes offrent des informations optionnelles, comme le navigateur utilisé. La dernière ligne, comme précisé plus haut, est caractérisée par deux sauts de ligne: textbackslash r \textbackslash n \textbackslash r \textbackslash n.





\newpage

\section{Exercice 3}
Il faut maintenant créer un client pour envoyer une requête HTTP GET à un serveur web et afficher la réponse. 

\subsection{Explication du code}
L'initialisation est similaire à celle de l'exercice 1. La création se fait tout de même avec SOCK\_STREAM, et la connection se fait par l'appel systemè \emph{connect()}.

\begin{lstlisting}
if(connect(sockfd, (struct sockaddr*) &destinationAddress, sizeof(destinationAddress)) == -1){
	perror("Client error: connecting to server");
	exit(EXIT_FAILURE);
}
\end{lstlisting}

Il faut ensuite créer une requête HTTP GET, ce à quoi sert \emph{createHTTP\_GETRequest}. Les deux premières lignes sont les mêmes que dans l'exercice précédent. La ligne "Connection: close" sert à fermer la connection pour pouvoir recevoir 0 lors de l'appel système \emph{read()}. Si cela nétait pas fait, l'appel ne retournerait pas de valeur.

\begin{lstlisting}
char* createHTTP_GETRequest(const char* host, const char* res, const char* port) {
	char *buff = malloc(BUFFER_LEN);

	int status = snprintf(buff, BUFFER_LEN,
		"GET %s HTTP/1.1\r\n"
		"Host: %s:%s\r\n"
		"Connection: close\r\n"
		"Accept: text/html\r\n\r\n",
		res, host, port
	);

	if(status < 0) {
		free(buff);
		return NULL;
	}

	return buff;
}
\end{lstlisting}


La requête est enfin envoyée avec \emph{sendMessage()}. Cette méthode est semblable à la méthode \emph{sendCompleteMessage()} décrite préccédemment. Enfin, le serveur affiche la réponse en appellant \emph{printReceivedRequest()}. Cette fonction appelle simplement \emph{read()} jusqu'à avoir reçu un message complet.

\newpage
\subsection{Traces d'exécution}

\begin{figure}[h!]
  \centering
  \includegraphics[width=1\linewidth]{exo3.png}
  \caption{Résultats de la ligne de commande}
\end{figure}


\newpage

\section{Exercice 4}

L'objectif de cet exercice est de créer un serveur web utilisant deux ports pour répondre à des requêtes HTTP ou afficher un fichier de logs.

Pour cela, deux sockets sont créées de la même manière que pour l'exercice 2. La fonction \emph{select()} permet d'alterner le traitement des requêtes, en utilisant le \emph{fd\_set readfs}. L'utilisation de \emph{select()} modifie \emph{readfs} et indique les fd qui sont lisibles.

Il reste ensuite à déterminer quelle socket possède des données pouvant être lues. La connection du client est acceptée, puis traitée. Si la socket concernée est celle gérant les HTTP GET, alors la fonction \emph{handleHTTP\_GETRequest} est appellée. Cette fonction reçoit une requête HTTP en appellant \emph{receiveGETRequest()}, puis log la requête, trouve le chemin demandé, et envoie le fichier en appellant \emph{sendFileByHTTP}.

La fonction \emph{receiveGETRequest()} est similaire aux fonctions déjà décrites permettant d'afficher une requête GET, mais renvoie un \emph{char*} au lieu de l'afficher.

La fonction \emph{logToFile} transforme l'addresse du client en char* avec \emph{inet\_ntoa}, récupère l'heure de réception avec \emph{localtime}, crée une string avec cela et le fichier requis, et enfin append cette string au fichier de log.

\begin{lstlisting}
int logToFile(struct in_addr clientAddress, char *res){
	int logfd;
	char dateString[200];
	char *clientAddressString = inet_ntoa(clientAddress);

	time_t t = time(NULL);
	struct tm *tmp = localtime(&t);
	if(tmp == NULL){
		perror("Error: could not get the local time");
		return -1;
	}

	if(strftime(dateString, 200, "%T", tmp) == 0){
		puts("Error: strftime: failed to write into dateString");
		return -1;
	}

	int stringSize = strlen(clientAddressString) + strlen(dateString) + strlen(res) + 8;
	char *str = malloc(stringSize);

	snprintf(str, stringSize,
		"%s - %s - %s\n",
		clientAddressString, dateString, res
	);

	logfd = open("log.txt", O_CREAT|O_WRONLY|O_APPEND, 0664);
	if(logfd == -1){
		perror("Error: could not open log file");
		free(str);
		return -1;
	}

	write(logfd, str, strlen(str));

	free(str);
	close(logfd);

	return 0;
}
\end{lstlisting}

La fonction \emph{sendFileByHTTP} vérifie si le fichier demandé peut être ouvert, et envoie une erreur 404 si \emph{open()} échoue. Sinon, une réponse positive est envoyée, puis le fichier lui même, en utilisant \emph{sendFile()}.

\begin{lstlisting}
nt sendFileByHTTP(char* filepath, int fd_dest){
	int filefd, status;
	char *contentType, *response;

	if(strstr(filepath, ".html") != NULL){
		contentType = TYPE_HTML;
	}
	else if(strstr(filepath, ".txt") != NULL){
		contentType = TYPE_PLAIN_TEXT;
	}else{
		contentType = TYPE_OCTET_STREAM;
	}

	filefd = open(filepath, O_RDONLY);
	if(filefd == -1){
		perror("Error opening required file");

		if(errno == ENOENT){
			sendMessage(fd_dest, HTTP_404_ERROR, strlen(HTTP_404_ERROR));
		}
		return -1;
	}

	response = genereateHTTP_200Response(contentType);
	if(response == NULL){
		close(filefd);
		return -1;
	}
	
	status = sendMessage(fd_dest, response, strlen(response));
	
	free(response);
	
	if(status == -1){
		close(filefd);
		return -1;
	}

	status = sendFile(filefd, fd_dest);

	close(filefd);

	return status;
}

\end{lstlisting}


Si la requête passe par la socket dédiée aux logs, alors le fichier de log est envoyé avec \emph{sendFileByHTTP}.
\newpage
\subsection{Traces d'exécution}

\begin{figure}[h!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.4\linewidth]{exo4_1.png}
  \caption{Echec d'une requête}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.4\linewidth]{exo4_2.png}
  \caption{Demande de lol.txt}
\end{subfigure}
\caption{Requêtes sur le port 400 dédié aux requêtes HTTP GET}
\label{fig:test}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=.4\linewidth]{exo4_3.png}
  \caption{Demande du fichier de log (Contenant aussi des connections précédentes)}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=1\linewidth]{exo4_4.png}
  \caption{Résultats de la ligne de commande}
\end{figure}






\end{document}
