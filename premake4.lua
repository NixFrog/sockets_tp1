solution "TP1"
	configurations ""
	project "1client"
		kind "ConsoleApp"
		language "C"
		files({ "src/exo1/client.h", "src/exo1/client.c", "src/exo1/tools.h", "src/exo1/tools.c" })
		--includedirs({"/", "includes"})

		flags({"Symbols", "ExtraWarnings"})
		links({})
		--libdirs({"Driver/"})

		buildoptions({"-Wextra"})
		linkoptions({})

	project "1server"
		kind "ConsoleApp"
		language "C"
		files({ "src/exo1/server.h", "src/exo1/server.c", "src/exo1/tools.h", "src/exo1/tools.c" })
		--includedirs({"/", "includes"})

		flags({"Symbols", "ExtraWarnings"})
		links({})
		--libdirs({"Driver/"})

		buildoptions({"-Wextra"})
		linkoptions({})

	project "2server"
		kind "ConsoleApp"
		language "C"
		files({ "src/exo2/server.h", "src/exo2/server.c"})
		--includedirs({"/", "includes"})

		flags({"Symbols", "ExtraWarnings"})
		links({})
		--libdirs({"Driver/"})

		buildoptions({"-Wextra"})
		linkoptions({})

	project "3client"
		kind "ConsoleApp"
		language "C"
		files({ "src/exo3/client.c"})
		--includedirs({"/", "includes"})

		flags({"Symbols", "ExtraWarnings"})
		links({})
		--libdirs({"Driver/"})

		buildoptions({"-Wextra"})
		linkoptions({})


	project "4server"
		kind "ConsoleApp"
		language "C"
		files({ "src/exo4/server.c"})
		--includedirs({"/", "includes"})

		flags({"Symbols", "ExtraWarnings"})
		links({})
		--libdirs({"Driver/"})

		buildoptions({"-Wextra"})
		linkoptions({})
